const converter = document.querySelector('#converter');
const result = document.querySelector('.result');
const convBtn = document.querySelector('.conv');
const resetBtn = document.querySelector('.reset');
const changeBtn = document.querySelector('.change');
const celsius = document.querySelector('.celsius');
const farenheit = document.querySelector('.farenheit');

const convFun = () => {
	if (celsius.textContent === '°C' && converter.value !== '') {
		result.textContent =
			[converter.value + '°C to '] + [converter.value * 1.8 + 32 + '°F'];
		result.style.color = '';
	} else if (celsius.textContent === '°F' && converter.value !== '') {
		result.textContent =
			[converter.value + '°F to '] + [(converter.value - 32) / 1.8 + '°C'];
		result.style.color = '';
	} else {
		result.style.color = 'red';
		result.textContent = 'Podana wartość jest nieprawidłowa! Wprowadź liczbę.';
	}
};

const switchSpan = () => {
	if (celsius.textContent === '°C') {
		celsius.textContent = '°F';
		farenheit.textContent = '°C';
	} else {
		celsius.textContent = '°C';
		farenheit.textContent = '°F';
	}
};

const reset = () => {
	converter.value = '';
	result.textContent = '';
};

convBtn.addEventListener('click', convFun);
changeBtn.addEventListener('click', switchSpan);
resetBtn.addEventListener('click', reset);
